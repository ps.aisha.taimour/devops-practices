package com.progressoft.devops.assignment.controller;

import com.progressoft.devops.assignment.repository.PersonEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PersonMapperTest {


    @Test
    void givenPerson_whenMap_thenShouldReturnPersonEntity() {
        long id = 1L;
        String name = "Aisha";
        int age = 10;
        Person person = new Person(id, name, age);

        PersonEntity personEntity = PersonMapper.toEntity(person);

        Assertions.assertEquals(id, personEntity.getId());
        Assertions.assertEquals(name, personEntity.getName());
        Assertions.assertEquals(age, personEntity.getAge());
    }


    @Test
    void givenPersonWithId_whenMap_thenShouldReturnPersonEntity() {
        long id = 1L;
        String name = "Aisha";
        int age = 10;
        Person person = new Person(id, name, age);

        long newId = 2L;
        PersonEntity personEntity = PersonMapper.toEntity(person, newId);

        Assertions.assertEquals(newId, personEntity.getId());
        Assertions.assertEquals(name, personEntity.getName());
        Assertions.assertEquals(age, personEntity.getAge());
    }


    @Test
    void givenPersonEntity_whenMap_thenShouldReturnPerson() {
        long id = 1L;
        String name = "Aisha";
        int age = 10;
        PersonEntity personEntity = new PersonEntity(id, name, age);

        Person person = PersonMapper.fromEntity(personEntity);

        Assertions.assertEquals(id, person.getId());
        Assertions.assertEquals(name, person.getName());
        Assertions.assertEquals(age, person.getAge());
    }
}